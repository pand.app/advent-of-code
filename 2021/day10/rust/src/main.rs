use std::collections::HashMap;
use std::env;
use std::io::{self, BufRead};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let pairs: HashMap<char, char> =
        HashMap::from([('(', ')'), ('[', ']'), ('{', '}'), ('<', '>')]);

    let input: Vec<Vec<char>> = io::stdin()
        .lock()
        .lines()
        .map(|l| l.unwrap().chars().collect())
        .collect();

    match env::args().nth(1).as_ref().map(|s| s.as_str()) {
        Some("part1") => {
            let score_list: HashMap<char, u32> =
                HashMap::from([(')', 3), (']', 57), ('}', 1197), ('>', 25137)]);

            let score = input.iter().fold(0, |s, line| {
                let mut stack: Vec<char> = vec![];
                for char in line {
                    if let Some(_) = pairs.get(char) {
                        stack.push(*char);
                    } else if char != pairs.get(&stack.pop().unwrap_or(' ')).unwrap_or(&' ') {
                        return s + score_list.get(char).unwrap_or(&0u32);
                    }
                }
                return s;
            });
            // expected output from ../input.txt : 294195
            print!("{}", score);
        }
        Some("part2") => {
            let score_list: HashMap<char, u32> =
                HashMap::from([(')', 1), (']', 2), ('}', 3), ('>', 4)]);
            let mut scores: Vec<usize> = input.iter().fold(vec![], |s, line| {
                let mut broken = false;
                let mut stack: Vec<char> = vec![];

                for char in line {
                    if pairs.contains_key(&char) {
                        stack.push(*char);
                    } else if let Some(c) = pairs.get(&stack.pop().unwrap()) {
                        if char != c {
                            broken = true;
                            break;
                        }
                    }
                }
                if !broken {
                    let mut new_vec: Vec<usize> = s.clone();
                    new_vec.push(stack.iter().rev().fold(0, |score, char| {
                        5 * score + *score_list.get(pairs.get(char).unwrap()).unwrap() as usize
                    }));
                    return new_vec;
                }
                s
            });
            scores.sort();
            let score = scores[(scores.len() as f64 / 2.0).floor() as usize];
            // expected output from ../input.txt : 3490802734
            print!("{}", score);
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
