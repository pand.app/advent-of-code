import { readAll } from "https://deno.land/std/streams/conversion.ts";
const stdin: string = new TextDecoder().decode(await readAll(Deno.stdin));

const CLOSEST = [
  [-1, -1],
  [-1, +0],
  [-1, +1],
  [+0, -1],
  [+0, +1],
  [+1, -1],
  [+1, +0],
  [+1, +1],
];

type Point = [number, number];
type Line = number[];
type Grid = Line[];

const [part] = Deno.args;

const data: Grid = stdin
  .split("\n").map((line) => line.split("").map((char) => parseInt(char, 10)));
const height = data.length;
const width = data[0].length;

function resetFlashed(grid: Grid) {
  grid.forEach((line, y) =>
    line.forEach((u, x) => {
      if (u >= 10) {
        grid[y][x] = 0;
      }
    })
  );
}

function flashPropagation(grid: Grid, flashed: Point[]): number {
  let sum = 0;
  while (flashed.length) {
    const [pointY, pointX] = flashed.pop()!;
    sum += 1;
    for (const [y, x] of CLOSEST) {
      const lookupY = pointY + y;
      if (lookupY < 0 || height <= lookupY) {
        continue;
      }
      const lookupX = pointX + x;
      if (lookupX < 0 || width <= lookupX) {
        continue;
      }
      data[lookupY][lookupX] += 1;
      if (data[lookupY][lookupX] === 10) {
        flashed.push([lookupY, lookupX]);
      }
    }
  }
  return sum;
}

function tick(grid: number[][]): Point[] {
  const flashed: Point[] = [];
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
      grid[y][x] += 1;
      if (grid[y][x] === 10) {
        flashed.push([y, x]);
      }
    }
  }
  return flashed;
}

switch (part) {
  case "part1": {
    let grid = data;
    let sum = 0;

    for (let step = 0; step < 100; step++) {
      const flashed = tick(grid);
      sum += flashPropagation(grid, flashed);
      resetFlashed(grid);
    }
    // expected output from ../input.txt : 1601
    await Deno.stdout.write(
      new TextEncoder().encode(
        sum.toFixed(),
      ),
    );
    break;
  }
  case "part2": {
    let grid = data;
    let step = 0;
    let sum = 0;
    do {
      step += 1;
      sum = 0;
      const flashed = tick(grid);
      sum += flashPropagation(grid, flashed);
      resetFlashed(grid);
    } while (sum !== height * width);
    // expected output from ../input.txt : 368
    await Deno.stdout.write(
      new TextEncoder().encode(
        step.toFixed(),
      ),
    );
    break;
  }
  default:
}
