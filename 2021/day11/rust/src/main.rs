use std::env;
use std::io::{self, BufRead};

const CLOSEST: [[isize; 2]; 8] = [
    [-1, -1],
    [-1, 0],
    [-1, 1],
    [0, -1],
    [0, 1],
    [1, -1],
    [1, 0],
    [1, 1],
];
type Line = Vec<u8>;

struct Grid {
    height: usize,
    width: usize,
    grid: Vec<Line>,
}

type Point = [usize; 2];

impl Grid {
    fn tick(&mut self) -> Vec<Point> {
        let mut flashed: Vec<Point> = vec![];
        for y in 0..self.height {
            for x in 0..self.width {
                self.grid[y][x] += 1;
                if self.grid[y][x] == 10 {
                    flashed.push([y, x]);
                }
            }
        }
        return flashed;
    }

    fn flash_propagation(&mut self, original_flashed: &Vec<Point>) -> u16 {
        let mut sum: u16 = 0;
        let mut flashed = original_flashed.to_vec();
        while flashed.len() > 0 {
            let [point_y, point_x] = flashed.pop().unwrap();
            sum += 1;
            for [y, x] in CLOSEST {
                let lookup_y = (point_y as isize + y) as usize;
                if self.height <= lookup_y {
                    continue;
                }
                let lookup_x = (point_x as isize + x) as usize;
                if self.width <= lookup_x {
                    continue;
                }
                self.grid[lookup_y][lookup_x] += 1;
                if self.grid[lookup_y][lookup_x] == 10 {
                    flashed.push([lookup_y, lookup_x]);
                }
            }
        }
        sum
    }

    fn reset_flashed(&mut self) {
        for y in 0..self.height {
            for x in 0..self.width {
                if self.grid[y][x] >= 10 {
                    self.grid[y][x] = 0;
                }
            }
        }
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input: Vec<Line> = io::stdin()
        .lock()
        .lines()
        .map(|line| {
            line.unwrap()
                .chars()
                .map(|n| n.to_digit(10).unwrap() as u8)
                .collect()
        })
        .collect();

    let mut grid = Grid {
        height: input.len(),
        width: input[0].len(),
        grid: input,
    };

    match env::args().nth(1).as_ref().map(|s| s.as_str()) {
        Some("part1") => {
            let sum = (0..100).fold(0, |acc, _| {
                let flashed = (&mut grid).tick();
                let sum = (&mut grid).flash_propagation(&flashed);
                (&mut grid).reset_flashed();
                acc + sum
            });
            // expected output from ../input.txt : 1601
            print!("{}", sum);
        }
        Some("part2") => {
            let mut step: usize = 0;
            let mut sum: usize;

            loop {
                step += 1;
                sum = 0;
                let flashed = (&mut grid).tick();
                sum += (&mut grid).flash_propagation(&flashed) as usize;
                (&mut grid).reset_flashed();
                if sum == grid.height * grid.width {
                    break;
                }
            }
            // expected output from ../input.txt : 368
            print!("{}", step);
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
