use std::cmp::max;
use std::env;
use std::io::{self, BufRead};
use std::num::ParseIntError;
use std::str::FromStr;

#[derive(Debug, Copy, Clone)]
struct Point {
    x: u16,
    y: u16,
    sum: u16,
}

impl FromStr for Point {
    type Err = ParseIntError;
    fn from_str(raw_point: &str) -> Result<Self, Self::Err> {
        let coordinates: Vec<&str> = raw_point.split(",").collect();
        Ok(Point {
            x: coordinates[0].trim().parse::<u16>().unwrap(),
            y: coordinates[1].trim().parse::<u16>().unwrap(),
            sum: 0,
        })
    }
}

type Coordinate = (Point, Point);

type Line = Vec<Point>;
type Matrix = Vec<Line>;

fn forward(coordinate: Coordinate) -> Line {
    let from = coordinate.0;
    let to = coordinate.1;
    let mut current = from;
    let mut path = vec![from];
    while current.x != to.x || current.y != to.y {
        if current.x < to.x {
            current.x += 1;
        } else if current.x > to.x {
            current.x -= 1;
        }

        if current.y < to.y {
            current.y += 1;
        } else if current.y > to.y {
            current.y -= 1;
        }
        path.push(current);
    }
    path
}

fn count_intersections_greater_than_two_lines(matrix: &Matrix) -> u16 {
    matrix
        .into_iter()
        .flatten()
        .fold(0, |acc, point| acc + if point.sum >= 2 { 1 } else { 0 })
}

fn increment_matrix(matrix: &mut Matrix, coordinates: Vec<Coordinate>) {
    for coordinate in coordinates {
        for point in forward(coordinate) {
            matrix[point.y as usize][point.x as usize].sum += 1;
        }
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let part = env::args().nth(1).unwrap();

    let input: Vec<Coordinate> = io::stdin()
        .lock()
        .lines()
        .map(|l| l.unwrap())
        .map(|l| {
            let parts: Vec<&str> = l.split("->").collect();
            (
                Point::from_str(parts[0]).unwrap(),
                Point::from_str(parts[1]).unwrap(),
            )
        })
        .collect();

    let matrix_length: u16 = input
        .iter()
        .flat_map(|p| vec![p.0, p.1])
        .fold(0, |acc, val| max(max(acc, val.x), val.y))
        + 1;
    let mut matrix: Matrix = Vec::with_capacity(matrix_length as usize);
    for y in 0..matrix_length {
        let mut line = Vec::with_capacity(matrix_length as usize);
        for x in 0..matrix_length {
            line.push(Point { x, y, sum: 0 });
        }
        matrix.push(line);
    }

    match part.as_ref() {
        "part1" => {
            let filtered_input: Vec<Coordinate> = input
                .iter()
                .filter(|r| r.0.y == r.1.y || r.0.x == r.1.x)
                .cloned()
                .collect();
            increment_matrix(&mut matrix, filtered_input);
            // expected output from ../input.txt : 7438
            print!("{}", count_intersections_greater_than_two_lines(&matrix));
        }
        "part2" => {
            increment_matrix(&mut matrix, input);
            // expected output from ../input.txt : 21406
            print!("{}", count_intersections_greater_than_two_lines(&matrix));
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
