import { readAll } from "https://deno.land/std/streams/conversion.ts";
const stdin: string = new TextDecoder().decode(await readAll(Deno.stdin));

const [part] = Deno.args;

function intersect(a: any[], b: any[]) {
  const setB = new Set(b);
  return [...new Set(a)].filter((x) => setB.has(x));
}

const data: string[][][][] = stdin
  .split("\n")
  .map((r) =>
    r.trim().split("|").map((l) =>
      l.trim().split(/\s+/).map((u) => u.split(""))
    )
  );

switch (part) {
  case "part1": {
    const sum = data.reduce(
      (count, [, output]) =>
        count +
        output.filter((r) => [2, 3, 4, 7].includes(r.length)).length,
      0,
    );
    // expected output from ../input.txt : 409
    await Deno.stdout.write(
      new TextEncoder().encode(
        sum.toFixed(),
      ),
    );
    break;
  }
  case "part2": {
    const sum = data.reduce((count, [input, output]) => {
      const one = input.find(({ length }) => length === 2) ?? [];
      const four = input.find(({ length }) => length === 4) ?? [];
      return count + output.reduce((acc, out) => {
        switch (out.length) {
          case 2:
            return acc * 10 + 1;
          case 3:
            return acc * 10 + 7;
          case 4:
            return acc * 10 + 4;

          case 5:
            if (intersect(out, one).length === 2) {
              return acc * 10 + 3;
            } else if (intersect(out, four).length === 2) {
              return acc * 10 + 2;
            }
            return acc * 10 + 5;
          case 6:
            if (intersect(out, four).length === 4) {
              return acc * 10 + 9;
            } else if (intersect(out, one).length === 2) {
              return acc * 10;
            }
            return acc * 10 + 6;
          case 7:
            return acc * 10 + 8;
          default:
            return acc * 10;
        }
      }, 0);
    }, 0);

    // expected output from ../input.txt : 1024649
    await Deno.stdout.write(
      new TextEncoder().encode(
        sum.toFixed(),
      ),
    );
    break;
  }
  default:
}
