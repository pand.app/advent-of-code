use std::env;
use std::io::{self, BufRead};
use std::rc::Rc;
use std::cell::RefCell;
type Line = Vec<u8>;
type Grid = Vec<Line>;
type Coordinate = [u16; 2];

const CLOSEST: [[isize; 2]; 4] = [[1, 0], [-1, 0], [0, 1], [0, -1]];

fn is_lowest(grid: &Grid, x: &u16, y: &u16) -> bool {
    let empty_vec = vec![];
    CLOSEST
        .iter()
        .map(|[mx, my]| {
            grid.get((*x as isize + mx) as usize)
                .unwrap_or(&empty_vec)
                .get((*y as isize + my) as usize)
                .unwrap_or(&u8::MAX)
        })
        .all(|o| grid[*x as usize][*y as usize] < *o)
}

fn get_lowest(grid: &Grid, dim: (u16, u16)) -> Vec<Coordinate> {
    (0..dim.0)
        .flat_map(|x| {
            (0..dim.1)
                .map(move |y| [x, y])
                .filter(|[x, y]| is_lowest(&grid, &x, &y))
        })
        .collect()
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input: Grid = io::stdin()
        .lock()
        .lines()
        .map(|l| l.unwrap())
        .map(|l| l.chars().map(|n| n.to_digit(10).unwrap() as u8).collect())
        .collect();

    let height = input.len() as u16;
    let width = input[0].len() as u16;

    match env::args().nth(1).as_ref().map(|s| s.as_str()) {
        Some("part1") => {
            let score: usize = get_lowest(&input, (width, height))
                .iter()
                .map(|[x, y]| (input[*x as usize][*y as usize] + 1) as usize)
                .sum();
            // expected output from ../input.txt : 554
            print!("{}", score);
        }
        Some("part2") => {
            let mut scores: Vec<u32> = get_lowest(&input, (width, height))
                .iter()
                .map(|[x, y]| {
                    let open: Rc<RefCell<Vec<Coordinate>>> = Rc::new(RefCell::new(vec![[*x, *y]]));
                    let close: Rc<RefCell<Vec<Coordinate>>> = Rc::new(RefCell::new(vec![]));
                    while open.borrow().len() > 0 {
                        let [row, col] = open.borrow_mut().swap_remove(0);
                        CLOSEST
                            .iter()
                            .map(|[y, x]| [row + *y as u16, col + *x as u16])
                            .filter(|[y, x]| {
                                0 <= *y
                                    && *y < height
                                    && 0 <= *x
                                    && *x < width
                                    && !close.clone().borrow().iter().any(|&r| r[0] == *y && r[1] == *x)
                                    && !open.clone().borrow().iter().any(|&r| r[0] == *y && r[1] == *x)
                                    && input[*y as usize][*x as usize] < 9
                            })
                            .for_each(|u| open.borrow_mut().push(u));
                        close.borrow_mut().push([row, col]);
                    }
                    close.clone().borrow().len() as u32
                })
                .collect();
            scores.sort_by(|a, b| b.cmp(a));
            print!("{:?}", scores[0..3].iter().product::<u32>());
            // expected output from ../input.txt : 1017792
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
