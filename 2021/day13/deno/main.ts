import { readAll } from "https://deno.land/std/streams/conversion.ts";
const stdin: string = new TextDecoder().decode(await readAll(Deno.stdin));

const [part] = Deno.args;

const [dotsInput, instructionsInput] = stdin
  .split("\n\n");

const instructions: [string, number][] = instructionsInput.split("\n").map((instruction: string) => {
  const [, direction, location] = instruction.match(/([x|y])=(\d+)/)!;
  return [direction, parseInt(location, 10)];
});

type DotsStruct =  { dots: number[][], width: number, height: number };
const { dots, width, height }: DotsStruct = dotsInput.split("\n").reduce((acc: DotsStruct, line) => {
  const [x, y] = line.split(",").map((p) => parseInt(p, 10));
  acc.width = Math.max(acc.width, x + 1);
  acc.height = Math.max(acc.height, y + 1);
  acc.dots.push([y, x]);
  return acc;
}, {dots: [], width: 0, height: 0} as DotsStruct);

const matrix: boolean[][] = dots.reduce((acc: boolean[][], [y, x ] ) => {
  acc[y][x] = true;
  return acc;
}, Array.from(
      { length: height },
      () => Array.from({length: width}, () => false),
  )
);

function foldMatrix(matrix: boolean[][], [direction, location]: [direction: string, location: number]) {
  const newMatrix = JSON.parse(JSON.stringify(matrix));
  switch (direction) {
    case 'x':
      for (let x = 0; x < location; x++) {
        for (let y = 0; y < newMatrix.length; y++) {
          if (newMatrix[y][2 * location - x]) {
            newMatrix[y][x] = newMatrix[y][x] || newMatrix[y][2 * location - x];
          }
        }
      }
      newMatrix.forEach((line: boolean[]) => { line.splice(location) });
      break;
    case 'y':
      for (let y = 0; y < location; y++) {
        for (let x = 0; x < newMatrix[0].length; x++) {
          if (newMatrix[2 * location - y]) {
            newMatrix[y][x] = newMatrix[y][x] || newMatrix[2 * location - y][x];
          }
        }
      }
      newMatrix.splice(location);
      break;
  }

  return newMatrix;
}

function logMatrix(matrix: boolean[][]) {
  return matrix.map((line) => line.map(c => c ? '#' : '.').join('')).join("\n");
}

function countMatrix(matrix: boolean[][]) {
  return matrix.reduce((acc, line) => acc + line.reduce((s, c) => s + +c, 0), 0);
}

switch (part) {
  case "part1": {
    const folded = foldMatrix(matrix, instructions[0]);
    // expected output from ../input.txt : 684
    await Deno.stdout.write(
      new TextEncoder().encode(
        countMatrix(folded).toFixed(),
      ),
    );
    break;
  }
  case "part2": {
    const folded = instructions.reduce(foldMatrix, matrix);
    // expected output from ../input.txt : JRZBLGKH
    await Deno.stdout.write(
      new TextEncoder().encode(
          logMatrix(folded)
      ),
    );
    break;
  }
  default:
}
