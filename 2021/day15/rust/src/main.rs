use std::cmp::Reverse;
use std::collections::{BinaryHeap, HashMap};
use std::env;
use std::io::{self, BufRead};

const CLOSEST: [[isize; 2]; 4] = [[1, 0], [-1, 0], [0, 1], [0, -1]];
const TIMES_LARGER: usize = 5;

type Coordinate = [usize; 2];
type Map = Vec<Vec<u8>>;
type PointInQueue = (Reverse<u32>, Coordinate);

fn best_path_cost(map: Map) -> u32 {
    let mut best: HashMap<Coordinate, u32> = HashMap::new();
    let mut queue: BinaryHeap<PointInQueue> = [(Reverse(0), [0, 0])].into();
    while let Some((Reverse(total_risk), [x, y])) = queue.pop() {
        let known_risk = best.entry([x, y]).or_insert(u32::MAX);
        if total_risk < *known_risk {
            *known_risk = total_risk;
            for [dx, dy] in CLOSEST {
                let (x, y) = (x as isize + dx, y as isize + dy);
                if x >= 0 && y >= 0 {
                    if let Some(line) = map.get(y as usize) {
                        if let Some(risk) = line.get(x as usize) {
                            queue.push((
                                Reverse(total_risk + *risk as u32),
                                [x as usize, y as usize],
                            ));
                        }
                    }
                }
            }
        }
    }
    best[best.keys().max().unwrap()]
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input: Map = io::stdin()
        .lock()
        .lines()
        .map(|line| {
            line.unwrap()
                .chars()
                .map(|c| c.to_digit(10).unwrap() as u8)
                .collect()
        })
        .collect();

    let height = input.len();
    let width = input[0].len();

    match env::args().nth(1).as_ref().map(|s| s.as_str()) {
        Some("part1") => {
            let lowest_cost = best_path_cost(input);
            // expected output from ../input.txt : 373
            print!("{}", lowest_cost);
        }
        Some("part2") => {
            let mut new_grid: Map = vec![vec![0; TIMES_LARGER * width]; TIMES_LARGER * height];
            for py in 0..TIMES_LARGER {
                for px in 0..TIMES_LARGER {
                    let add = (py + px) as u8;
                    for (y, line) in input.iter().enumerate() {
                        for (x, risk) in line.iter().enumerate() {
                            new_grid[y + py * height][x + px * width] = (risk + add - 1) % 9 + 1;
                        }
                    }
                }
            }
            let lowest_cost = best_path_cost(new_grid);
            // expected output from ../input.txt : 2868
            print!("{}", lowest_cost);
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
