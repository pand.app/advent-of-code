import { readAll } from "https://deno.land/std/streams/conversion.ts";
const stdin: string = new TextDecoder().decode(await readAll(Deno.stdin));

type ShortBinary = (0 | 1)[];

const data: ShortBinary[] = stdin
  .split("\n")
  .filter((r) => r.length > 0)
  .map((r) => r.split("").map((n) => parseInt(n, 10)) as ShortBinary);

const guessLength = data[0].length;

function sumBinaries(data: ShortBinary[]) {
  return data.reduce(
    (acc, row) => acc.map((val, index) => val + row[index]),
    Array.from({ length: guessLength }).map((_) => 0) as number[],
  );
}

const [part] = Deno.args;

switch (part) {
  case "part1": {
    const halfDataLength = data.length / 2;

    const binarySum = sumBinaries(data);
    const epsilonRate = parseInt(
      binarySum.map((r) => r < halfDataLength ? 1 : 0).reduce((acc, val) =>
        acc + val, ""),
      2,
    );
    const gammaRate = parseInt(
      binarySum.map((r) => r > halfDataLength ? 1 : 0).reduce((acc, val) =>
        acc + val, ""),
      2,
    );

    const total = epsilonRate * gammaRate;
    // expected output from ../input.txt : 3374136
    await Deno.stdout.write(
      new TextEncoder().encode(
        total.toFixed(),
      ),
    );
    break;
  }
  case "part2": {
    let O2Rating = [...data];
    let CO2Rating = [...data];
    for (let i = 0; i < guessLength; i++) {
      if (O2Rating.length > 1) {
        const O2Index = sumBinaries(O2Rating)[i];

        O2Rating = O2Rating.filter((r) =>
          r[i] ===
            ((O2Index === O2Rating.length / 2)
              ? 1
              : (O2Index > O2Rating.length / 2 ? 1 : 0))
        );
      }
      if (CO2Rating.length > 1) {
        const CO2Index = sumBinaries(CO2Rating)[i];
        CO2Rating = CO2Rating.filter((r) =>
          r[i] ===
            ((CO2Index === CO2Rating.length / 2)
              ? 0
              : (CO2Index < CO2Rating.length / 2 ? 1 : 0))
        );
      }
    }
    const decimalO2Rating = parseInt(
      O2Rating.reduce((acc, val) => acc.concat(val), []).reduce((acc, val) =>
        acc + val, ""),
      2,
    );
    const decimalCO2Rating = parseInt(
      CO2Rating.reduce((acc, val) => acc.concat(val), []).reduce((acc, val) =>
        acc + val, ""),
      2,
    );

    const total = decimalO2Rating * decimalCO2Rating;

    // expected output from ../input.txt : 4432698
    await Deno.stdout.write(
      new TextEncoder().encode(
        total.toFixed(),
      ),
    );
    break;
  }
  default:
}
