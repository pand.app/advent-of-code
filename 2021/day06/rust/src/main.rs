use std::env;
use std::io::{self, BufRead};

fn tick(population: Vec<usize>) -> Vec<usize> {
    let mut new_pop = population.to_vec();
    new_pop.rotate_left(1);
    new_pop[6] += population[0];
    new_pop
}

fn compute(input: Vec<usize>, days: usize) -> usize {
    vec![0; days]
        .iter()
        .fold(
            input
                .iter()
                .fold(vec![0; 9], |population: Vec<usize>, current: &usize| {
                    let mut new_pop = population.to_vec();
                    new_pop[*current] += 1;
                    new_pop
                }),
            |acc, _| tick(acc),
        )
        .iter()
        .sum()
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input: Vec<usize> = io::stdin()
        .lock()
        .lines()
        .next()
        .unwrap()?
        .split(",")
        .filter_map(|s| s.parse().ok())
        .collect();

    match env::args().nth(1).as_ref().map(|s| s.as_str()) {
        Some("part1") => {
            // expected output from ../input.txt : 374994
            print!("{}", compute(input, 80));
        }
        Some("part2") => {
            // expected output from ../input.txt : 1686252324092
            print!("{}", compute(input, 256));
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
