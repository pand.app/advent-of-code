import { readAll } from "https://deno.land/std/streams/conversion.ts";
const stdin: string = new TextDecoder().decode(await readAll(Deno.stdin));

const data: number[] = stdin
  .split("\n")
  .filter((r) => r.trim().length > 0)
  .join()
  .split(",")
  .map((u) => parseInt(u, 10));

const [part] = Deno.args;

function tick(population: number[]) {
  const first = population.shift() ?? 0;
  const newPopulation = [...population, first];
  newPopulation[6] += first;
  return newPopulation;
}

function compute(input: number[], days: number): number {
  return Array.from({ length: days })
    .reduce(
      (acc: number[], _) => tick(acc),
      input.reduce((population, current) => {
        population[current] += 1;
        return population;
      }, Array.from({ length: 9 }).map((_) => 0)),
    )
    .reduce((total, number) => total + number, 0);
}

switch (part) {
  case "part1": {
    // expected output from ../input.txt : 374994
    await Deno.stdout.write(
      new TextEncoder().encode(
        compute(data, 80).toFixed(),
      ),
    );
    break;
  }
  case "part2": {
    // expected output from ../input.txt : 1686252324092
    await Deno.stdout.write(
      new TextEncoder().encode(
        compute(data, 256).toFixed(),
      ),
    );
    break;
  }
  default:
}
