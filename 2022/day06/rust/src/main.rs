use std::collections::HashSet;
use std::env;
use std::io::{self, BufRead};
use std::iter::FromIterator;

#[derive(Debug)]
struct Buffer(String);
impl Buffer {
    fn get_packet_starting_index(&self, marker_size: u8) -> u16 {
        let chars: Vec<char> = self.0.chars().collect();
        for (index, set) in chars.windows(marker_size.into()).enumerate() {
            let set: HashSet<&char> = HashSet::from_iter(set);
            if set.len() == marker_size.into() {
                return index as u16 + marker_size as u16;
            }
        }
        panic!("No packet start found");
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let part = env::args().nth(1).unwrap();

    let buffer = Buffer(
        io::stdin()
            .lock()
            .lines()
            .filter_map(|e| e.ok())
            .next()
            .unwrap(),
    );

    match part.as_ref() {
        "part1" => {
            // expected output from ../input.txt : 1175
            print!("{}", buffer.get_packet_starting_index(4));
        }
        "part2" => {
            // expected output from ../input.txt : 3217
            print!("{}", buffer.get_packet_starting_index(14));
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
