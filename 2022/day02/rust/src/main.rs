use std::env;
use std::io::{self, BufRead};

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
enum Choice {
    Rock,
    Paper,
    Scissors,
}

enum Expectation {
    Win,
    Lose,
    Draw,
}

impl Choice {
    fn from_initial(letter: &char) -> Choice {
        match letter {
            'A' => Choice::Rock,
            'B' => Choice::Paper,
            'C' => Choice::Scissors,
            _ => panic!("letter not supported"),
        }
    }

    fn from_response(letter: &char) -> Choice {
        match letter {
            'X' => Choice::Rock,
            'Y' => Choice::Paper,
            'Z' => Choice::Scissors,
            _ => panic!("letter not supported"),
        }
    }

    fn expected_opponent(&self, expectation: &Expectation) -> Choice {
        match expectation {
            Expectation::Draw => self.clone(),
            Expectation::Win => match self {
                Choice::Rock => Choice::Paper,
                Choice::Paper => Choice::Scissors,
                Choice::Scissors => Choice::Rock,
            },
            Expectation::Lose => match self {
                Choice::Rock => Choice::Scissors,
                Choice::Paper => Choice::Rock,
                Choice::Scissors => Choice::Paper,
            },
        }
    }

    fn get_value(&self) -> u8 {
        match self {
            Choice::Rock => 1,
            Choice::Paper => 2,
            Choice::Scissors => 3,
        }
    }
    fn won(&self, against: &Choice) -> bool {
        (self == &Choice::Rock && against == &Choice::Scissors)
            || (self == &Choice::Scissors && against == &Choice::Paper)
            || (self == &Choice::Paper && against == &Choice::Rock)
    }

    fn score(&self, against: &Choice) -> u8 {
        let mut score = self.get_value();
        if self == against {
            score += 3;
        }
        if self.won(against) {
            score += 6
        }
        score
    }
}

impl Expectation {
    fn from_letter(letter: &char) -> Expectation {
        match letter {
            'X' => Expectation::Lose,
            'Y' => Expectation::Draw,
            'Z' => Expectation::Win,
            _ => panic!("letter not supported"),
        }
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let part = env::args().nth(1).unwrap();

    let list = io::stdin()
        .lock()
        .lines()
        .filter_map(|e| e.ok())
        .map(|line| {
            let (a, b) = line.split_once(" ").unwrap();
            (a.chars().nth(0).unwrap(), b.chars().nth(0).unwrap())
        });

    match part.as_ref() {
        "part1" => {
            // expected output from ../input.txt : 15422
            let score = list
                .map(|line| {
                    let (adversary, myself) = line;
                    let adversary_choice = Choice::from_initial(&adversary);
                    let my_choice = Choice::from_response(&myself);
                    my_choice.score(&adversary_choice) as u32
                })
                .reduce(|a, b| a + b)
                .unwrap();
            print!("{score}");
        }
        "part2" => {
            // expected output from ../input.txt : 15442
            let score = list
                .map(|line| {
                    let (adversary, expectation) = line;
                    let adversary_choice = Choice::from_initial(&adversary);
                    let my_choice =
                        adversary_choice.expected_opponent(&Expectation::from_letter(&expectation));
                    my_choice.score(&adversary_choice) as u32
                })
                .reduce(|a, b| a + b)
                .unwrap();
            print!("{score}");
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
