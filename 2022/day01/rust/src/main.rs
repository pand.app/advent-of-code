use std::env;
use std::io::{self, BufRead};

#[derive(Default, Debug)]
struct Elf {
    calories: Vec<u16>,
    sum: u32,
}

#[derive(Debug)]
struct Tribe {
    elves: Vec<Elf>,
}

impl Tribe {
    fn get_max_calories(&self) -> u32 {
        self.elves
            .iter()
            .fold(0, |acc, elf| if elf.sum > acc { elf.sum } else { acc })
    }
    fn get_top3_max_calories(&self) -> u32 {
        let mut top3: Vec<u32> = (0..3).map(|_| 0).collect();
        for elf in self.elves.iter() {
            top3.sort_by(|a, b| b.cmp(a));
            if elf.sum > *top3.first().unwrap() {
                top3.pop();
                top3.push(elf.sum);
            }
        }
        top3.iter().sum()
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let part = env::args().nth(1).unwrap();

    let tribe = Tribe {
        elves: io::stdin().lock().lines().filter_map(|e| e.ok()).fold(
            vec![Elf::default()],
            |mut acc, num| {
                if num == "" {
                    acc.push(Elf::default());
                    return acc;
                }
                let parsed: u16 = num.parse().unwrap();
                let current_elf = acc.last_mut().unwrap();
                current_elf.calories.push(parsed);
                current_elf.sum += parsed as u32;
                acc
            },
        ),
    };
    // println!("{:?}", tribe);

    match part.as_ref() {
        "part1" => {
            // expected output from ../input.txt : 69206
            print!("{}", tribe.get_max_calories());
        }
        "part2" => {
            // expected output from ../input.txt : 197400
            print!("{}", tribe.get_top3_max_calories());
        }
        _ => panic!("first param should be `part1` or `part2`"),
    };
    Ok(())
}
